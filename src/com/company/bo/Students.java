package com.company.bo;

public class Students {
    private int rollNumber;
    private String name;
    private String batch;
    private String section;
    private DegreeType degree;

    public Students(int rollNumber, String name, String batch,String section,DegreeType degree){
        setRollNumber(rollNumber);
        setName(name);
        setBatch(batch);
        setSection(section);
        setDegree(degree);
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDegree(DegreeType degree) {
        this.degree = degree;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public DegreeType getDegree() {
        return degree;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public String getBatch() {
        return batch;
    }

    public String getName() {
        return name;
    }

    public String getSection() {
        return section;
    }
}
