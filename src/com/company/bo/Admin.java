package com.company.bo;

import com.company.exception.BookNotAvailableException;

public class Admin {
    private String name;
    private String identityNumber;

    public Admin(String name, String identifyNumber){
        setName(name);
        setIdentityNumber(identifyNumber);
    }

    public void setName(String name){
        this.name=name;
    }

    public void setIdentityNumber(String identityNumber){
        this.identityNumber=identityNumber;
    }

    public String getName(){
        return name;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }
}
