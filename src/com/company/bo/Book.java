package com.company.bo;

public class Book {
    private String name;
    private SubjectType subject;
    private int totalCount;
    private int issuedBookCount;


    public Book(){

    }
    public Book(String name, SubjectType subject){
        setName(name);
        setSubject(subject);

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubject(SubjectType subject) {
        this.subject = subject;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public void setIssuedBookCount(int issuedBookCount) {
        this.issuedBookCount = issuedBookCount;
    }

    public SubjectType getSubject() {
        return subject;
    }

    public String getName() {
        return name;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getIssuedBookCount() {
        return issuedBookCount;
    }
}
