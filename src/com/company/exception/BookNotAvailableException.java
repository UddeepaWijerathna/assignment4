package com.company.exception;

public class BookNotAvailableException extends RuntimeException {
    public BookNotAvailableException(){
        super("Available Book Count is Zero");
    }
}
